class Board {
  rows = 10;
  cols = 10;
  boardContents = [];
  table = null;
  tableCells = null;
  container = null;
  spaceOver = [-1, -1];
  isPlayers = true; //If true, this board is for the human player, else it is for the CPU

  constructor(container, isPlayers, rows, cols) {
    let t = this;

    t.rows = rows;
    t.cols = cols;
    t.isPlayers = isPlayers;

    //Initialise the board
    t.container = container;
    t.table = document.createElement("table");
    if (!t.isPlayers) {
      t.table.id = "cpu-board";
    } else {
      t.table.id = "player-board";
    }
    let tr = this.table.insertRow();
    let td = tr.insertCell(-1);
    td.colSpan = t.rows + 1;
    td.innerHTML = isPlayers ? "Player's board" : "CPU's board";

    t.tableCells = [];
    for (let i = 0; i < this.rows + 1; i++) {
      tr = this.table.insertRow();
      t.tableCells[i] = [];
      for (let j = 0; j < this.cols + 1; j++) {
        td = tr.insertCell(-1);

        if (i === 0 && j > 0) {
          //Add column numbers
          td.innerHTML = j - 1;
        } else if (i > 0 && j === 0) {
          //Row letters
          td.innerHTML = String.fromCharCode(65 + (i - 1));
        } else {
          td.className = "boardSpace";
          if (!this.isPlayers) {
            td.onmouseenter = function() {
              t.spaceOver = [i - 1, j - 1];
            };
          }
        }

        t.tableCells[i][j] = td;
      }
    }
    t.container.append(this.table);

    for (let row = 0; row < this.rows; row++) {
      t.boardContents[row] = [];
      for (let col = 0; col < this.cols; col++) {
        t.boardContents[row][col] = ".";
      }
    }

    if (!this.isPlayers) {
      t.table.onmouseleave = function() {
        t.spaceOver = [-1, -1];
      }
    }
  }

  //Getters
  get rows() {
    return this.rows;
  }

  get cols() {
    return this.cols;
  }

  get boardContents() {
    return this.boardContents;
  }

  get spaceOver() {
    return this.spaceOver;
  }

  //Methods
  /**
   * Gets the status of a grid space on the board
   * @param row Row to check
   * @param col Column to check
   * @return "X" if hit, "O" if missed, "." if space has not been fired on
   */
  getSpace(row, col) {
    if (row >= 0 && row < this.rows && col >= 0 && col < this.cols) {
      return this.boardContents[row][col];
    }
    return "";
  }

  /**
   * Sets a space on the board to a status
   * @param row Row to set
   * @param col Column to set
   * @param status Status to set (hit or miss)
   * @return Nothing
   */
  setSpace(row, col, status) {
    row = parseInt(row);
    col = parseInt(col);
    if (status.toLowerCase() == "hit") {
      this.boardContents[row][col] = "X";
      this.tableCells[row + 1][col + 1].innerHTML = "●";
    } else if (status.toLowerCase() == "miss") {
      this.boardContents[row][col] = "O";
      this.tableCells[row + 1][col + 1].innerHTML = "○";
    } else {
      console.log("Board.setSpace: Invalid status set.");
    }
  }

  /**
   * Places the ships on the board (all placed horizontally for now)
   * @param shipArray Array of ships to place
   * @return Nothing
   */
   placeShips (shipArray) {
     const nShips = shipArray.length; //Number of ships
     let freeRows = [];
     //let freeSpaces = new Array(this.rows * this.cols);
     //let startSpace;
     let freeSpaces = [];
     let randRow, randCol = null; //Random row/column to place ship

     /*for (let i = this.rows - 1; i >= 0; i--) {
       freeRows[i] = i;
     }*/
     //for (let i = freeSpaces.length - 1; i >= 0; i--) {
    //   freeSpaces[i] = [Math.floor(i / this.cols), i % this.cols];
     //}
     for (let i = this.rows - 1; i >= 0; i--) {
       freeSpaces[i] = [];
       for (let j = this.cols - 1; j >= 0; j--) {
         freeSpaces[i][j] = true;
       }
     }


     for (let ship = 0; ship < nShips; ship++) {
       //Ship length is equal to its hit points
       let size = shipArray[ship].size;
       let isVertical = Math.floor(Math.random() * 2) == 1 ? true : false;
       shipArray[ship].hp = size;
       shipArray[ship].isVertical = isVertical;

       //startSpace = Math.floor(Math.random() * freeSpaces.length);

       //Place ship on a random, empty row
       //randRow = freeRows[Math.floor(Math.random() * Math.floor(freeRows.length))];

       //freeRows.splice(freeRows.indexOf(randRow), 1);

       /* Place ship on a random column, making sure no part of it is placed off
       the edge of the board */
       //randCol = Math.floor(Math.random() * (Math.floor(this.cols) - (size - 1)));
       //randCol = Math.floor(Math.random() * (Math.floor(freeSpaces[i].length) - (size - 1)));

       //shipArray[ship].row = randRow;
       //shipArray[ship].col = randCol;

       //console.log(`Placing ship "${shipArray[ship].name}" with size ${shipArray[ship].size} at space [${shipArray[ship].row}, ${shipArray[ship].col}]...`);

       //Place each part of the ship
       let shipPlacedSuccessfully = false;
       while (!shipPlacedSuccessfully) {
         randRow = Math.floor(Math.random() * Math.floor(this.rows - (isVertical ? size - 1 : 0)));
         randCol = Math.floor(Math.random() * Math.floor(freeSpaces[randRow].length - (isVertical ? size - 1 : 0)));
         //let spacesTaken = [];
         let sizeCounter = 0;

         //if (!isVertical) {
           for (let i = 0; i < size && ((!isVertical && freeSpaces[randRow][randCol + i] === true) || (isVertical && freeSpaces[randRow + i][randCol] === true)); i++) {
             //spacesTaken.push([randRow, randCol + x]);
             sizeCounter++;
           }
         /*} else {
           for (let y = 0; y < size && freeSpaces[randRow + y][randCol] === true; y++) {
             //spacesTaken.push([randRow + y, randCol]);
           }
         }*/

         if (sizeCounter === size) {
           shipPlacedSuccessfully = true;
           shipArray[ship].row = randRow;
           shipArray[ship].col = randCol;

           for (let i = 0; i < size; i++) {
             if (!isVertical) {
               this.boardContents[randRow][randCol + i] = "o";
               freeSpaces[randRow][randCol + i] = false;
             } else {
               this.boardContents[randRow + i][randCol] = "o";
               freeSpaces[randRow + i][randCol] = false;
             }

             if (this.isPlayers) {
               let td;
               if (!isVertical) {
                 td = this.tableCells[randRow + 1][randCol + i + 1];
               } else {
                 td = this.tableCells[randRow + i + 1][randCol + 1];
               }

               td.style.borderTop = isVertical && i >= 1 ? "none" : "1px solid lime";
               td.style.borderRight = !isVertical && i < size - 1 ? "none" : "1px solid lime";
               td.style.borderBottom = isVertical && i < size - 1 ? "none" : "1px solid lime";
               td.style.borderLeft = !isVertical && i >= 1 ? "none" : "1px solid lime";
               td.style.color = "lime";
               td.innerHTML = "○";
             }
           }
         }
       }

       /*for (let col = 0; col < size; col++) {
         this.boardContents[shipArray[ship].row][shipArray[ship].col + col] = "o";

         //Show the ship's location on the player's board
         if (this.isPlayers) {
           const td = this.tableCells[shipArray[ship].row + 1][shipArray[ship].col + col + 1];
           td.style.color = "lime";
           td.innerHTML = "○";
         }
       }*/
     }
   }

   /**
    * Clears all ships and shots off the board
    * @return Nothing
    */
   reset () {
     for (let i = 0; i < this.rows; i++) {
       for (let j = 0; j < this.cols; j++) {
         let tc = this.tableCells[i + 1][j + 1];
         this.boardContents[i][j] = ".";
         tc.innerHTML = " ";
         tc.style.color = "";
         tc.style.border = '1px solid gray';
       }
     }
   }

   /**
    * Sets the function to run when a space on the CPU's board is clicked
    * @param func Function to set
    * @return Nothing
    */
    setClickHandler(func) {
      if (!this.isPlayers) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.cols; j++) {
            this.tableCells[i + 1][j + 1].onclick = func;
          }
        }
      }
    }
}
