//Get element references
const container = document.getElementById("container");
const gameSection = document.getElementById("game");
const shipsLeftSpan = [document.getElementById("shipCount0"), document.getElementById("shipCount1")];
const shipTypesSpan = document.getElementById("shipTypes");
const messages = document.getElementById("messages");
const endMessageBack = document.getElementById("endMessageBack");
const endMessage = document.getElementById("endMessage");
const btnPlayAgain = document.getElementById("btnPlayAgain");

//Size of board
const nRows = 10;
const nCols = 10;

//Create the ships for each player
let ships = [["Patrol Boat", 2], ["Destroyer", 3], ["Submarine", 3], ["Battleship", 4], ["Carrier", 5]];
for (let i = 0; i < ships.length; i++) {
  shipTypes.append(`${ships[i][0]}: ${ships[i][1]} | `);
}
let playerShips = [];
for (let i = 0; i < 2; i++) {
  playerShips[i] = [];
  for (var j = 0; j < ships.length; j++) {
    playerShips[i][j] = new Ship(ships[j][0], ships[j][1]);
  }
}

//Amount of ships that haven't been sunk
let shipsLeft = [ships.length, ships.length];

//Board spaces the CPU has not fired on
let openSpaces = [];
let openSpaceIdx;
resetCPUOpenSpaces();

//Other ships the CPU player has hit but not yet sunk
let cpuShipsToSink = new Map();

//Location of CPU player's first hit on a ship
let cpuFirstHitPos = null;

//Index of ship that the CPU last hit
let cpuShipLastHitIdx = null;

//Whether or not the CPU player has only made one hit on a ship
// (used for finding the rest of a ship after the first hit)
let cpuFirstHit = false;

//Location of CPU player's shot
let shotRow = -1;
let shotCol = -1;

//Outcome of the CPU player's last shot (see description of fireShot())
let cpuShot;

//Create the boards for the player and the CPU
const board2 = new Board(gameSection, true, nRows, nCols);
const boardCPU = new Board(gameSection, false, nRows, nCols);

//Start the game
initGame(true);

/**
 * Makes a shot at the row and column specified
 * @param shipArray Ship array (for names and hp)
 * @param board Board to fire on
 * @return Status code (0: Invalid shot, 1: Position already fired on, 2: Ship hit, 3: Ship sunk, 4: Missed)
 */
function fireShot(player, shipArray, board, row, col) {
  let pName = player === 0 ? "Player" : "CPU";
  if (row < 0 || row > board.rows - 1 || col < 0 || col > board.cols - 1) {
    //Coordinates are off the board
    addMessage(`Invalid position entered: [${row}, ${col}]`);
    return 0;
  } else if (board.getSpace(row, col) == "X" || board.getSpace(row, col) == "O") {
    //Position has been fired upon previously
    addMessage(`Position ${rowNumber2Letter(row)}${col} has already been fired on.`);
    return 1;
  } else {
    if (board.getSpace(row, col) == "o") {
      //Hit
      let ship = Ship.getShipAtPos(shipArray, row, col);
      if (ship != null) {
        ship.damage(1);
        board.setSpace(row, col, "hit");
        if (ship.hp > 0) {
          addMessage(`${pName} hit the ${ship.name} at ${rowNumber2Letter(row)}${col}. It has ${ship.hp} hp remaining.`);
          return 2;
        } else {
          //Ship has been sunk
          let otherPlayer = player == 0 ? 1 : 0;
          addMessage(`${pName} sunk the ${ship.name}!`);
          shipsLeft[otherPlayer]--;
          shipsLeftSpan[otherPlayer].innerHTML = shipsLeft[otherPlayer];
          return 3;
        }
      }
    } else {
      //Miss
      board.setSpace(row, col, "miss");
      addMessage(`${pName} shot missed: ${rowNumber2Letter(row)}${col}`);
      return 4;
    }
  }
}

/**
 * Adds a message to the message list
 * @param msg The message to add
 * @return Nothing
 */
function addMessage(msg) {
  const m = document.createElement("option");
  messages.prepend(m);
  m.innerHTML = msg;
}

/**
 * Shows a game over message in place of the game board
 * @param isVictory Did the player win?
 * @return Nothing
 */
 function gameEnd(isVictory) {
   if (isVictory) {
     endMessage.innerHTML = "Congratulations! You Won! You destroyed all the enemy ships.";
   } else {
     endMessage.innerHTML = "Game over! All your ships were sunk.";
   }
   endMessageBack.style.display = "flex";
 }

/**
 * Places each player's ships on the board and sets up the UI
 * @param firstTime True if the game is being run for the first time (i.e.
 *                  no need to clear current ships off the board and reset their HP)
 * @return nothing
 */
function initGame(firstTime) {
  shipsLeft = [ships.length, ships.length];
  if (!firstTime) {
    for (let i = 0; i < ships.length; i++) {
      playerShips[0][i].hp = playerShips[0][i].size;
      playerShips[1][i].hp = playerShips[1][i].size;
    }
    resetCPUOpenSpaces();
    board2.reset();
    boardCPU.reset();
  }
  board2.placeShips(playerShips[0]);
  boardCPU.placeShips(playerShips[1]);
  shipsLeftSpan[0].innerHTML = shipsLeft[0];
  shipsLeftSpan[1].innerHTML = shipsLeft[1];
  messages.innerHTML = "";
}

/**
 * Converts a row number into a letter (e.g. row 0 = A, 1 = B and so on)
 * @param row Row number to convert as an integer
 * @return Row letter or empty string if row is invalid
 */
function rowNumber2Letter(row) {
  if (Number.isSafeInteger(row) && row >= 0 && row < nRows) {
    return String.fromCharCode(65 + row);
  }
  return "";
}

/**
 * Resets the list of spaces that the CPU hasn't yet fired on
 * @return nothing
 */
function resetCPUOpenSpaces() {
  for (let i = 0; i < nRows * nCols; i++) {
    openSpaces[i] = [Math.floor(i / nCols), i % nCols];
  }
}

/**
 * Click handlers for 'Play Again' button and spaces on CPU player's board
 */
btnPlayAgain.onclick = function () {
  endMessageBack.style.display = "none";
  initGame(false);
}

boardCPU.setClickHandler(function() {
  let playerShot = fireShot(0, playerShips[1], boardCPU, boardCPU.spaceOver[0], boardCPU.spaceOver[1]);
  if (playerShot >= 2) {
    //CPU fires after player makes a valid move
    if (cpuFirstHitPos == null && cpuShipsToSink.size == 0) {
      //The CPU player has not yet hit one of the player's ships, so find a
      //space on the board that hasn't been fired on
      openSpaceIdx = Math.floor(Math.random() * openSpaces.length);
      shotRow = openSpaces[openSpaceIdx][0];
      shotCol = openSpaces[openSpaceIdx][1];
    } else {
      if (cpuFirstHit) {
        //Fire in a random adjacent space in an attempt to find the rest of the ship
        shotRow = cpuFirstHitPos[0];
        shotCol = cpuFirstHitPos[1];
        let possibleNextShots = [];
        let sp = board2.getSpace(shotRow - 1, shotCol);
        if (shotRow > 0 && sp !== "" && sp !== 'X' && sp !== 'O') { //North
          possibleNextShots.push([shotRow - 1, shotCol]);
        }

        sp = board2.getSpace(shotRow, shotCol + 1);
        if (shotCol < nCols - 1 && sp !== "" && sp !== 'X' && sp !== 'O') { //East
          possibleNextShots.push([shotRow, shotCol + 1]);
        }

        sp = board2.getSpace(shotRow + 1, shotCol);
        if (shotRow < nRows - 1 && sp !== "" && sp !== 'X' && sp !== 'O') { //South
          possibleNextShots.push([shotRow + 1, shotCol]);
        }

        sp = board2.getSpace(shotRow, shotCol - 1);
        if (shotCol > 0  && sp !== "" && sp !== 'X' && sp !== 'O') { //West
          possibleNextShots.push([shotRow, shotCol - 1]);
        }
        let idx = Math.floor(Math.random() * possibleNextShots.length);
        shotRow = possibleNextShots[idx][0];
        shotCol = possibleNextShots[idx][1];
      } else {
        /*Now that the direction of the ship is known, take out the rest of it.
          When the CPU misses a shot, reaches the edge of the board or finds a
          space that its already fired on, it will return to the position where
          it first hit the ship and then fire in the opposite direction to which
          it was previously firing*/
        let sp;
        if (shotRow < cpuFirstHitPos[0]) {
          sp = board2.getSpace(shotRow - 1, shotCol);
          if (shotRow > 0 && cpuShot === 2 && sp !== 'X' && sp !== 'O') {
            shotRow--;
          } else {
            shotRow = cpuFirstHitPos[0] + 1;
          }
        } else if (shotRow > cpuFirstHitPos[0]) {
          sp = board2.getSpace(shotRow + 1, shotCol);
          if (shotRow < nRows - 1 && cpuShot === 2 && sp !== 'X' && sp !== 'O') {
            shotRow++;
          } else {
            shotRow = cpuFirstHitPos[0] - 1;
          }
        } else if (shotCol < cpuFirstHitPos[1]) {
          sp = board2.getSpace(shotRow, shotCol - 1);
          if (shotCol > 0 && cpuShot === 2 && sp !== 'X' && sp !== 'O') {
            shotCol--;
          } else {
            shotCol = cpuFirstHitPos[1] + 1;
          }
        } else if (shotCol > cpuFirstHitPos[1]) {
          sp = board2.getSpace(shotRow, shotCol + 1);
          if (shotCol < nCols - 1 && cpuShot === 2 && sp !== 'X' && sp !== 'O') {
            shotCol++;
          } else {
            shotCol = cpuFirstHitPos[1] - 1;
          }
        }
      }
      for (let i = 0; i < openSpaces.length; i++) {
        if (openSpaces[i][0] === shotRow && openSpaces[i][1] === shotCol) {
          openSpaceIdx = i;
          break;
        }
      }
    }
    openSpaces.splice(openSpaceIdx, 1);


    cpuShot = fireShot(1, playerShips[0], board2, shotRow, shotCol);
    let shipHitIndex = Ship.getShipIndexAtPos(playerShips[0], shotRow, shotCol);
    if (cpuShot === 2) {
      //CPU hit a player's ship
      if (!cpuShipsToSink.has(shipHitIndex)) {
        cpuShipsToSink.set(shipHitIndex, [shotRow, shotCol]);
      }

      if (cpuFirstHitPos == null) {
        cpuFirstHit = true;
        cpuFirstHitPos = [shotRow, shotCol];
      } else {
        if (cpuShipLastHitIdx === shipHitIndex) {
          cpuFirstHit = false;
        } else {
          cpuFirstHit = playerShips[0][shipHitIndex].hp === playerShips[0][shipHitIndex].size - 1;
          if (cpuFirstHit) {
            cpuFirstHitPos = [shotRow, shotCol];
          }
        }
      }

      cpuShipLastHitIdx = shipHitIndex;

    } else if (cpuShot === 3) {
      //CPU has sunk a player's ship
      cpuShipsToSink.delete(shipHitIndex);
      if (cpuShipsToSink.size > 0) {
        //Finish off any ships that have been previously hit
        let shipIdx = Array.from(cpuShipsToSink.keys()).pop();
        cpuFirstHitPos = [cpuShipsToSink.get(shipIdx)[0], cpuShipsToSink.get(shipIdx)[1]];
        cpuFirstHit = playerShips[0][shipIdx].hp === playerShips[0][shipIdx].size - 1;
      } else {
        //Return to random-fire if there are no ships that have been hit previously
        cpuFirstHit = false;
        cpuFirstHitPos = null;
      }
    }
  }

  if (shipsLeft[0] <= 0 && shipsLeft[1] > 0) {
    //Player loses (no player ships left + at least 1 CPU ship left)
    gameEnd(false);
  } else if (shipsLeft[0] > 0 && shipsLeft[1] <= 0) {
    //Player wins (at least 1 player ship left + no CPU ships left)
    gameEnd(true);
  }
});
