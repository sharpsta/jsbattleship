class Ship
{
  name = "New Ship";

  //Size of ship and starting HP
  size = 3;
  hp = this.size;

  //Starting row and column
  row = 0;
  col = 0;

  isVertical = false;

  constructor(name, size) {
    this.name = name;
    this.size = size;
    this.hp = size;
  }

  //Getters & setters
  get name() {
    return this.name;
  }

  get size() {
    return this.size;
  }

  set hp(hp) {
    this.hp = hp;
  }
  get hp() {
    return this.hp;
  }

  set row(row) {
    this.row = row;
  }
  get row() {
    return this.row;
  }

  set col(col) {
    this.col = col;
  }
  get col() {
    return this.col;
  }

  get isVertical() {
    return this.isVertical;
  }

  //Methods

  /**
   * Gets a ship object at the specified position, if one is there
   * @param shipArray Array of ship objects
   * @param row Row to search
   * @param col Column to search
   * @return Ship object if one is found at the position, else null
   */
  static getShipAtPos(shipArray, row, col) {
    for (let i = 0; i < shipArray.length; i++) {
      let ship = shipArray[i];
      if (!ship.isVertical && row == ship.row && (col >= ship.col && col < ship.col + ship.size) ||
          ship.isVertical && col == ship.col && (row >= ship.row && row < ship.row + ship.size)) {
        //Return ship object if a ship is at the specified position
        return ship;
      }
    }
    return null;
  }

  /**
   * Gets the index of a ship in an array of player's ships at a specific location if one is there
   * @param shipArray Array of ship objects
   * @param row Row to search
   * @param col Column to search
   * @return Array index of ship if found, else -1
   */
  static getShipIndexAtPos(shipArray, row, col) {
    for (let i = 0; i < shipArray.length; i++) {
      let ship = shipArray[i];
      if (!ship.isVertical && row == ship.row && (col >= ship.col && col < ship.col + ship.size) ||
          ship.isVertical && col == ship.col && (row >= ship.row && row < ship.row + ship.size)) {
        //Return ship object if a ship is at the specified position
        return i;
      }
    }
    return -1;
  }

  /**
   * Reduces a ship's hit points. Sets hp to zero if negative
   * @param hp Amount of hp to reduce
   * @return Nothing
   */
  damage(hp) {
    this.hp -= hp;
    if (this.hp < 0) {
      this.hp = 0;
    }
  }
}
